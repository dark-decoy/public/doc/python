## WSGI options
* uWSGI
* Gunicorn

## ASGI options
* Daphne
* Hypercorn
* Uvicorn

Run this before deploying to check for issues
```bash
python3 manage.py check --deploy
```

Create requirements.txt file with the following command
```bash
pip3 freeze > requirements.txt
```

## Sources
* https://docs.djangoproject.com/en/5.0/howto/deployment/
* https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment
