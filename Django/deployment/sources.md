# Python Dajango Deployment Sources

## Deployment
* [How to deploy Django \| Django documentation \| Django](https://docs.djangoproject.com/en/5.0/howto/deployment/ "How to deploy Django | Django documentation | Django")
* [Deploying Django to Production · Django Beats](https://fly.io/django-beats/deploying-django-to-production/ "Deploying Django to Production · Django Beats")
* [Deployment checklist \| Django documentation \| Django](https://docs.djangoproject.com/en/5.0/howto/deployment/checklist/ "Deployment checklist | Django documentation | Django")
* [Securely Deploy a Django App With Gunicorn\, Nginx\, \& HTTPS – Real Python](https://realpython.com/django-nginx-gunicorn/ "Securely Deploy a Django App With Gunicorn, Nginx, \& HTTPS – Real Python")
* [How to use Django with uWSGI \| Django documentation \| Django](https://docs.djangoproject.com/en/5.0/howto/deployment/wsgi/uwsgi/ "How to use Django with uWSGI | Django documentation | Django")
* [Quickly deploying a Django app with uWSGI and NGINX \| by Acacio Leclercqz \| Medium](https://medium.com/@leclercqz.acacio/quickly-deploying-a-django-app-with-uwsgi-and-nginx-5fee98c1c745 "Quickly deploying a Django app with uWSGI and NGINX | by Acacio Leclercqz | Medium")

## PEP 8
* [PEP 8\: The Style Guide for Python Code](https://pep8.org/ "PEP 8: The Style Guide for Python Code")

## MVC
* [The Model View Controller Pattern – MVC Architecture and Frameworks Explained](https://www.freecodecamp.org/news/the-model-view-controller-pattern-mvc-architecture-and-frameworks-explained/ "The Model View Controller Pattern – MVC Architecture and Frameworks Explained")
* [MVC \- MDN Web Docs Glossary\: Definitions of Web\-related terms \| MDN](https://developer.mozilla.org/en-US/docs/Glossary/MVC "MVC - MDN Web Docs Glossary: Definitions of Web-related terms | MDN")
* [MVC Architecture in 5 minutes\: a tutorial for beginners](https://www.educative.io/blog/mvc-tutorial "MVC Architecture in 5 minutes: a tutorial for beginners")
